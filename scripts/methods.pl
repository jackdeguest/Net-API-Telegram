sub answerCallbackQuery
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter callback_query_id" ) ) if( !exists( $opts->{ 'callback_query_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'answerCallbackQuery',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method answerCallbackQuery: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub answerInlineQuery
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter inline_query_id" ) ) if( !exists( $opts->{ 'inline_query_id' } ) );
	return( $self->error( "Missing parameter results" ) ) if( !exists( $opts->{ 'results' } ) );
	return( $self->error( "Value provided for results is not an array reference." ) ) if( ref( $opts->{ 'results' } ) ne 'ARRAY' );
	return( $self->error( "Value provided is not an array of either of this objects: InlineQueryResult" ) ) if( !$self->_param_check_array_object( qr/^(?:Net::API::Telegram::InlineQueryResult)$/, @_ ) );
    $self->_load( [qw( Net::API::Telegram::InlineQueryResult )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'answerInlineQuery',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method answerInlineQuery: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub answerPreCheckoutQuery
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter ok" ) ) if( !exists( $opts->{ 'ok' } ) );
	return( $self->error( "Missing parameter pre_checkout_query_id" ) ) if( !exists( $opts->{ 'pre_checkout_query_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'answerPreCheckoutQuery',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method answerPreCheckoutQuery: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub answerShippingQuery
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter ok" ) ) if( !exists( $opts->{ 'ok' } ) );
	return( $self->error( "Value provided for shipping_options is not an array reference." ) ) if( length( $opts->{ 'shipping_options' } ) && ref( $opts->{ 'shipping_options' } ) ne 'ARRAY' );
	return( $self->error( "Value provided is not an array of either of this objects: ShippingOption" ) ) if( length( $opts->{ 'shipping_options' } ) && !$self->_param_check_array_object( qr/^(?:Net::API::Telegram::ShippingOption)$/, @_ ) );
	return( $self->error( "Missing parameter shipping_query_id" ) ) if( !exists( $opts->{ 'shipping_query_id' } ) );
    $self->_load( [qw( Net::API::Telegram::ShippingOption )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'answerShippingQuery',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method answerShippingQuery: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub createNewStickerSet
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter emojis" ) ) if( !exists( $opts->{ 'emojis' } ) );
	return( $self->error( "Value provided for mask_position is not a Net::API::Telegram::MaskPosition object." ) ) if( length( $opts->{ 'mask_position' } ) && ref( $opts->{ 'mask_position' } ) ne 'Net::API::Telegram::MaskPosition' );
	return( $self->error( "Missing parameter name" ) ) if( !exists( $opts->{ 'name' } ) );
	return( $self->error( "Missing parameter png_sticker" ) ) if( !exists( $opts->{ 'png_sticker' } ) );
	return( $self->error( "Value provided for png_sticker is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'png_sticker' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Missing parameter title" ) ) if( !exists( $opts->{ 'title' } ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
    $self->_load( [qw( Net::API::Telegram::MaskPosition Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'createNewStickerSet',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method createNewStickerSet: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub deleteChatPhoto
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'deleteChatPhoto',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method deleteChatPhoto: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub deleteChatStickerSet
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'deleteChatStickerSet',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method deleteChatStickerSet: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub deleteMessage
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter message_id" ) ) if( !exists( $opts->{ 'message_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'deleteMessage',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method deleteMessage: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub deleteStickerFromSet
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter sticker" ) ) if( !exists( $opts->{ 'sticker' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'deleteStickerFromSet',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method deleteStickerFromSet: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub deleteWebhook
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'deleteWebhook',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method deleteWebhook: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub editMessageCaption
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'editMessageCaption',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method editMessageCaption: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub editMessageLiveLocation
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter latitude" ) ) if( !exists( $opts->{ 'latitude' } ) );
	return( $self->error( "Missing parameter longitude" ) ) if( !exists( $opts->{ 'longitude' } ) );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'editMessageLiveLocation',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method editMessageLiveLocation: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub editMessageMedia
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter media" ) ) if( !exists( $opts->{ 'media' } ) );
	return( $self->error( "Value provided for media is not a Net::API::Telegram::InputMedia object." ) ) if( ref( $opts->{ 'media' } ) ne 'Net::API::Telegram::InputMedia' );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InputMedia Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'editMessageMedia',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method editMessageMedia: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub editMessageReplyMarkup
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'editMessageReplyMarkup',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method editMessageReplyMarkup: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub editMessageText
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
	return( $self->error( "Missing parameter text" ) ) if( !exists( $opts->{ 'text' } ) );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'editMessageText',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method editMessageText: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub exportChatInviteLink
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'exportChatInviteLink',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method exportChatInviteLink: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{result} );
	}
}

sub forwardMessage
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter from_chat_id" ) ) if( !exists( $opts->{ 'from_chat_id' } ) );
	return( $self->error( "Value provided for from_chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'from_chat_id' } ) );
	return( $self->error( "Missing parameter message_id" ) ) if( !exists( $opts->{ 'message_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'forwardMessage',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method forwardMessage: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getChat
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getChat',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getChat: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Chat', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Chat object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getChatAdministrators
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getChatAdministrators',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getChatAdministrators: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $arr = [];
		foreach my $h ( @{$hash->{result}} )
		{
			my $o = $self->_response_to_object( 'Net::API::Telegram::ChatMember', $h ) ||
			return( $self->error( "Unable to create an Net::API::Telegram::ChatMember object with this data returned: ", sub{ $self->dumper( $h ) } ) );
			push( @$arr, $o );
		}
		return( $arr );
	}
}

sub getChatMember
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getChatMember',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getChatMember: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::ChatMember', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::ChatMember object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getChatMembersCount
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getChatMembersCount',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getChatMembersCount: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{result} );
	}
}

sub getFile
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter file_id" ) ) if( !exists( $opts->{ 'file_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getFile',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getFile: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::File', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::File object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getGameHighScores
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getGameHighScores',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getGameHighScores: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $arr = [];
		foreach my $h ( @{$hash->{result}} )
		{
			my $o = $self->_response_to_object( 'Net::API::Telegram::GameHighScore', $h ) ||
			return( $self->error( "Unable to create an Net::API::Telegram::GameHighScore object with this data returned: ", sub{ $self->dumper( $h ) } ) );
			push( @$arr, $o );
		}
		return( $arr );
	}
}

sub getMe
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getMe',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getMe: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::User', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::User object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getStickerSet
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter name" ) ) if( !exists( $opts->{ 'name' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getStickerSet',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getStickerSet: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::StickerSet', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::StickerSet object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getUpdates
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Value provided for allowed_updates is not an array reference." ) ) if( length( $opts->{ 'allowed_updates' } ) && ref( $opts->{ 'allowed_updates' } ) ne 'ARRAY' );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getUpdates',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getUpdates: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $arr = [];
		foreach my $h ( @{$hash->{result}} )
		{
			my $o = $self->_response_to_object( 'Net::API::Telegram::Update', $h ) ||
			return( $self->error( "Unable to create an Net::API::Telegram::Update object with this data returned: ", sub{ $self->dumper( $h ) } ) );
			push( @$arr, $o );
		}
		return( $arr );
	}
}

sub getUserProfilePhotos
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getUserProfilePhotos',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getUserProfilePhotos: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::UserProfilePhotos', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::UserProfilePhotos object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub getWebhookInfo
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'getWebhookInfo',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method getWebhookInfo: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::WebhookInfo', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::WebhookInfo object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub kickChatMember
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'kickChatMember',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method kickChatMember: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub leaveChat
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'leaveChat',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method leaveChat: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub pinChatMessage
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter message_id" ) ) if( !exists( $opts->{ 'message_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'pinChatMessage',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method pinChatMessage: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub promoteChatMember
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'promoteChatMember',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method promoteChatMember: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub restrictChatMember
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter permissions" ) ) if( !exists( $opts->{ 'permissions' } ) );
	return( $self->error( "Value provided for permissions is not a Net::API::Telegram::ChatPermissions object." ) ) if( ref( $opts->{ 'permissions' } ) ne 'Net::API::Telegram::ChatPermissions' );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
    $self->_load( [qw( Net::API::Telegram::ChatPermissions )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'restrictChatMember',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method restrictChatMember: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub sendAnimation
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter animation" ) ) if( !exists( $opts->{ 'animation' } ) );
	return( $self->error( "Value provided for animation is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'animation' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Value provided for thumb is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( length( $opts->{ 'thumb' } ) && ref( $opts->{ 'thumb' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InputFile Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendAnimation',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendAnimation: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendAudio
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter audio" ) ) if( !exists( $opts->{ 'audio' } ) );
	return( $self->error( "Value provided for audio is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'audio' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Value provided for thumb is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( length( $opts->{ 'thumb' } ) && ref( $opts->{ 'thumb' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InputFile Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendAudio',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendAudio: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendChatAction
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter action" ) ) if( !exists( $opts->{ 'action' } ) );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendChatAction',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendChatAction: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub sendContact
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter first_name" ) ) if( !exists( $opts->{ 'first_name' } ) );
	return( $self->error( "Missing parameter phone_number" ) ) if( !exists( $opts->{ 'phone_number' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendContact',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendContact: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendDocument
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter document" ) ) if( !exists( $opts->{ 'document' } ) );
	return( $self->error( "Value provided for document is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'document' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Value provided for thumb is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( length( $opts->{ 'thumb' } ) && ref( $opts->{ 'thumb' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InputFile Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendDocument',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendDocument: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendGame
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter game_short_name" ) ) if( !exists( $opts->{ 'game_short_name' } ) );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendGame',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendGame: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendInvoice
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter currency" ) ) if( !exists( $opts->{ 'currency' } ) );
	return( $self->error( "Missing parameter description" ) ) if( !exists( $opts->{ 'description' } ) );
	return( $self->error( "Missing parameter payload" ) ) if( !exists( $opts->{ 'payload' } ) );
	return( $self->error( "Missing parameter prices" ) ) if( !exists( $opts->{ 'prices' } ) );
	return( $self->error( "Value provided for prices is not an array reference." ) ) if( ref( $opts->{ 'prices' } ) ne 'ARRAY' );
	return( $self->error( "Value provided is not an array of either of this objects: LabeledPrice" ) ) if( !$self->_param_check_array_object( qr/^(?:Net::API::Telegram::LabeledPrice)$/, @_ ) );
	return( $self->error( "Missing parameter provider_token" ) ) if( !exists( $opts->{ 'provider_token' } ) );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
	return( $self->error( "Missing parameter start_parameter" ) ) if( !exists( $opts->{ 'start_parameter' } ) );
	return( $self->error( "Missing parameter title" ) ) if( !exists( $opts->{ 'title' } ) );
    $self->_load( [qw( Net::API::Telegram::LabeledPrice Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendInvoice',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendInvoice: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendLocation
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter latitude" ) ) if( !exists( $opts->{ 'latitude' } ) );
	return( $self->error( "Missing parameter longitude" ) ) if( !exists( $opts->{ 'longitude' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendLocation',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendLocation: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendMediaGroup
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter media" ) ) if( !exists( $opts->{ 'media' } ) );
	return( $self->error( "Value provided for media is not an array reference." ) ) if( ref( $opts->{ 'media' } ) ne 'ARRAY' );
	return( $self->error( "Value provided is not an array of either of this objects: InputMediaPhoto, InputMediaVideo" ) ) if( !$self->_param_check_array_object( qr/^(?:Net::API::Telegram::InputMediaPhoto|Net::API::Telegram::InputMediaVideo)$/, @_ ) );
    $self->_load( [qw( Net::API::Telegram::InputMediaPhoto Net::API::Telegram::InputMediaVideo )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendMediaGroup',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendMediaGroup: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $arr = [];
		foreach my $h ( @{$hash->{result}} )
		{
			my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $h ) ||
			return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
			push( @$arr, $o );
		}
		return( $arr );
	}
}

sub sendMessage
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Missing parameter text" ) ) if( !exists( $opts->{ 'text' } ) );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendMessage',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendMessage: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendPhoto
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter photo" ) ) if( !exists( $opts->{ 'photo' } ) );
	return( $self->error( "Value provided for photo is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'photo' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
    $self->_load( [qw( Net::API::Telegram::InputFile Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendPhoto',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendPhoto: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendPoll
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter options" ) ) if( !exists( $opts->{ 'options' } ) );
	return( $self->error( "Value provided for options is not an array reference." ) ) if( ref( $opts->{ 'options' } ) ne 'ARRAY' );
	return( $self->error( "Missing parameter question" ) ) if( !exists( $opts->{ 'question' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendPoll',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendPoll: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendSticker
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Missing parameter sticker" ) ) if( !exists( $opts->{ 'sticker' } ) );
	return( $self->error( "Value provided for sticker is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'sticker' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendSticker',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendSticker: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendVenue
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter address" ) ) if( !exists( $opts->{ 'address' } ) );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter latitude" ) ) if( !exists( $opts->{ 'latitude' } ) );
	return( $self->error( "Missing parameter longitude" ) ) if( !exists( $opts->{ 'longitude' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Missing parameter title" ) ) if( !exists( $opts->{ 'title' } ) );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendVenue',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendVenue: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendVideo
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Value provided for thumb is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( length( $opts->{ 'thumb' } ) && ref( $opts->{ 'thumb' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Missing parameter video" ) ) if( !exists( $opts->{ 'video' } ) );
	return( $self->error( "Value provided for video is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'video' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendVideo',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendVideo: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendVideoNote
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Value provided for thumb is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( length( $opts->{ 'thumb' } ) && ref( $opts->{ 'thumb' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
	return( $self->error( "Missing parameter video_note" ) ) if( !exists( $opts->{ 'video_note' } ) );
	return( $self->error( "Value provided for video_note is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'video_note' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendVideoNote',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendVideoNote: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub sendVoice
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a valid object. I was expecting one of the following: InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply" ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) !~ /^(?:Net::API::Telegram::InlineKeyboardMarkup|Net::API::Telegram::ReplyKeyboardMarkup|Net::API::Telegram::ReplyKeyboardRemove|Net::API::Telegram::ForceReply)$/ );
	return( $self->error( "Missing parameter voice" ) ) if( !exists( $opts->{ 'voice' } ) );
	return( $self->error( "Value provided for voice is not a valid object. I was expecting one of the following: InputFile, String" ) ) if( ref( $opts->{ 'voice' } ) !~ /^(?:Net::API::Telegram::InputFile)$/ );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup Net::API::Telegram::ReplyKeyboardMarkup Net::API::Telegram::ReplyKeyboardRemove Net::API::Telegram::ForceReply Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'sendVoice',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method sendVoice: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) ||
		return( $self->error( "Unable to create an Net::API::Telegram::Message object with this data returned: ", sub{ $self->dumper( $h ) } ) );
		return( $o );
	}
}

sub setChatDescription
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setChatDescription',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setChatDescription: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setChatPermissions
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter permissions" ) ) if( !exists( $opts->{ 'permissions' } ) );
	return( $self->error( "Value provided for permissions is not a Net::API::Telegram::ChatPermissions object." ) ) if( ref( $opts->{ 'permissions' } ) ne 'Net::API::Telegram::ChatPermissions' );
    $self->_load( [qw( Net::API::Telegram::ChatPermissions )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setChatPermissions',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setChatPermissions: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setChatPhoto
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter photo" ) ) if( !exists( $opts->{ 'photo' } ) );
	return( $self->error( "Value provided for photo is not a Net::API::Telegram::InputFile object." ) ) if( ref( $opts->{ 'photo' } ) ne 'Net::API::Telegram::InputFile' );
    $self->_load( [qw( Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setChatPhoto',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setChatPhoto: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setChatStickerSet
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter sticker_set_name" ) ) if( !exists( $opts->{ 'sticker_set_name' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setChatStickerSet',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setChatStickerSet: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setChatTitle
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter title" ) ) if( !exists( $opts->{ 'title' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setChatTitle',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setChatTitle: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setGameScore
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter score" ) ) if( !exists( $opts->{ 'score' } ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setGameScore',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setGameScore: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setPassportDataErrors
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter errors" ) ) if( !exists( $opts->{ 'errors' } ) );
	return( $self->error( "Value provided for errors is not an array reference." ) ) if( ref( $opts->{ 'errors' } ) ne 'ARRAY' );
	return( $self->error( "Value provided is not an array of either of this objects: PassportElementError" ) ) if( !$self->_param_check_array_object( qr/^(?:Net::API::Telegram::PassportElementError)$/, @_ ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
    $self->_load( [qw( Net::API::Telegram::PassportElementError )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setPassportDataErrors',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setPassportDataErrors: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub setWebhook
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
    $opts->{certificate} = Net::API::Telegram::InputFile->new( $self->{ssl_cert} ) if( $opts->{certificate} && $self->{ssl_cert} );
	return( $self->error( "Value provided for allowed_updates is not an array reference." ) ) if( length( $opts->{ 'allowed_updates' } ) && ref( $opts->{ 'allowed_updates' } ) ne 'ARRAY' );
	return( $self->error( "Value provided for certificate is not a Net::API::Telegram::InputFile object." ) ) if( length( $opts->{ 'certificate' } ) && ref( $opts->{ 'certificate' } ) ne 'Net::API::Telegram::InputFile' );
	return( $self->error( "Missing parameter url" ) ) if( !exists( $opts->{ 'url' } ) );
    $self->_load( [qw( Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'setWebhook',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method setWebhook: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub stopMessageLiveLocation
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'stopMessageLiveLocation',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method stopMessageLiveLocation: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Message', $hash->{result} ) || 
		return( $self->error( "Error while getting an object out of hash for this message: ", $self->error ) );
		return( $o );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub stopPoll
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter message_id" ) ) if( !exists( $opts->{ 'message_id' } ) );
	return( $self->error( "Value provided for reply_markup is not a Net::API::Telegram::InlineKeyboardMarkup object." ) ) if( length( $opts->{ 'reply_markup' } ) && ref( $opts->{ 'reply_markup' } ) ne 'Net::API::Telegram::InlineKeyboardMarkup' );
    $self->_load( [qw( Net::API::Telegram::InlineKeyboardMarkup )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'stopPoll',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method stopPoll: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::Poll', $hash->{result} ) || 
		return( $self->error( "Error while getting a Poll object out of hash for this message: ", $self->error ) );
		return( $o );
	}
}

sub unbanChatMember
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'unbanChatMember',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method unbanChatMember: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub unpinChatMessage
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter chat_id" ) ) if( !exists( $opts->{ 'chat_id' } ) );
	return( $self->error( "Value provided for chat_id is not a valid value. I was expecting one of the following: Integer, String" ) ) if( !length( $opts->{ 'chat_id' } ) );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'unpinChatMessage',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method unpinChatMessage: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	else
	{
		return( $hash->{ok} );
	}
}

sub uploadStickerFile
{
	my $self = shift( @_ );
	my $opts = $self->_param2hash( @_ ) || return( undef() );
	return( $self->error( "Missing parameter png_sticker" ) ) if( !exists( $opts->{ 'png_sticker' } ) );
	return( $self->error( "Value provided for png_sticker is not a Net::API::Telegram::InputFile object." ) ) if( ref( $opts->{ 'png_sticker' } ) ne 'Net::API::Telegram::InputFile' );
	return( $self->error( "Missing parameter user_id" ) ) if( !exists( $opts->{ 'user_id' } ) );
    $self->_load( [qw( Net::API::Telegram::InputFile )] ) || return( undef() );
	my $form = $self->_options2form( $opts );
	my $hash = $self->query({
		'method' => 'uploadStickerFile',
		'data' => $form,
	}) || return( $self->error( "Unable to make post query for method uploadStickerFile: ", $self->error->message ) );
	if( my $t_error = $self->_has_telegram_error( $hash ) )
	{
		return( $self->error( $t_error ) );
	}
	elsif( $hash->{result} )
	{
		my $o = $self->_response_to_object( 'Net::API::Telegram::File', $hash->{result} ) || 
		return( $self->error( "Error while getting a File object out of hash for this message: ", $self->error ) );
		return( $o );
	}
}

